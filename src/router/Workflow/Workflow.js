import React, { Component } from 'react';

import Tabs from '../../ui/components/Tabs/Tabs';
import Tab from '../../ui/components/Tabs/Tab';
import Select from '../../ui/components/Select/Select';

const optionSelect = ['All', 'Progress', 'Time spent', 'Status', 'Deadline', 'Value'];

class Workflow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabs: 0
    };
  }

  handleChange = (index) => {
    this.setState({ tabs: index });
  }

  render() {
    return (
      <main className="workflow">
        <header className="section-header">
          <Tabs onChange={this.handleChange}>
            <Tab label="All Projects (358)" />
            <Tab label="Workflow " />
          </Tabs>
          <Select option={optionSelect} handleChange={this.filterMassage} />
        </header>
      </main>
    );
  }
}

export default Workflow;