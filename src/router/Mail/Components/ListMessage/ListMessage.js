import React, { Component } from 'react';
import './list-message.css';

import Message from './components/Message';


class ListMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      massageIndex: 0
    };
  }

  dialogIndex = (index) => {
    return this.props.index === index;
  }

  handleChange = (callback, index) => {
    this.setState({
      massageIndex: index
    }, () => {
      callback(index);
    });
  }


  render() {
    return (
      <div className="messages-list">
        <ul>
          {this.props.data.map((user, index) => {
            const lastMessage = user.messages[user.messages.length - 1];
            return (
              <Message
                key={user.id}
                user={user}
                activeClass={this.dialogIndex(index)}
                message={lastMessage.text}
                date={lastMessage.date}
                onClick={this.handleChange.bind(this, this.props.onClick, index)} />
            );
          })}
        </ul>
      </div>
    );
  }
}

export default ListMessage;