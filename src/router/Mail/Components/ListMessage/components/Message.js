import React from 'react';
import './message.css';


const Message = (props) => (
  <li className={props.activeClass ? 'message__item active' : 'message__item'} onClick={props.onClick}>
    <div className="message__header">
      <span className={props.user.online ? "message__status online": "message__status ofline"}></span>
      <div className="message__user">
        <img className="message__photo" src={props.user.img} alt="" />
        <span className="message__name">{props.user.name}</span>
      </div>
      <span className={props.user.read ? "message__date read" : "message__date"}>{dateFormat(props.date)}</span>
    </div>
    <p className="message__text">{wordCount(props.message)}</p>
  </li>
);

function wordCount(word) {
  return word.length >= 80 ?  word.substring(0, 80) + '...' : word;
}


const trans = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Ноябрь',
  'Декабрь'
];

function dateFormat(date) {
  const now = new Date();
  const dateMessage = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  const curentDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());

  if (dateMessage.getTime() === curentDate.getTime()) {
    return `Сегодня ${date.toLocaleTimeString()}`;
  } else if (dateMessage.getTime() === curentDate.getTime() - 86400000) {
    return `Вчера ${date.toLocaleTimeString()}`;
  } else { // сообщение пришло позавчера или позже
    return date.getDate() + " " + trans[date.getMonth()];
  }
}



export default Message;