import React, { Component } from 'react';
import ViewMessage from '../ViewMessage/ViewMessage';
import Delete from 'react-icons/lib/md/delete';
import './chat.css';

const BtnDelete = (props) => (
  <div onClick={props.onClick} className="chat-delete">
    <Delete size={20} />
  </div>
);

class Chat extends Component {
  handleSubmit = (newMessage, event) => {
    event.preventDefault();
    newMessage(this.refs.chatInput);
  }

  componentDidMount() {
    const chatBlock = this.refs.chatView;
    this.props.scrollChat(chatBlock);
  }

  render() {
    return (
      <div className="chat">
       {this.props.typeChat !== 'trash' ? <BtnDelete onClick={this.props.deleteMessage}/> : null}
        <div ref="chatView" className="chat-content">
          <ViewMessage
            data={this.props.messages}
            img={this.props.img}
          />
          {this.props.typing()}
        </div>
        <form className="chat-form" onSubmit={this.handleSubmit.bind(this, this.props.newMessage)}>
          <input className="chat-input" type="text" ref='chatInput' placeholder="Введите сообщение" />
        </form>
      </div>
    );
  }
}

export default Chat;