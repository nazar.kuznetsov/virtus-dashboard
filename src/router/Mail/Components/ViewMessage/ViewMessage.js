import React from 'react';
import User from '../../../../assets/img/chat/user.webp';

const ViewMessage = (props) => (
  <ul className="chat-message__list">

    {props.data.map((item, index) => {
      return (
        <li key={index} className={item.bot ? 'chat-message__item sent' : 'chat-message__item'}>
          <img className="chat-message__photo" src={item.bot ? props.img : User} alt="" />
          <div>
            <p className="message-chat__text">{item.text}</p>
            <p className="chat-message__time">{item.date}</p>
          </div>
        </li>
      );
    })}
  </ul>
);

export default ViewMessage;