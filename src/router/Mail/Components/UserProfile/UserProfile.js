import React from 'react';
import './user-profile.css';

const UserProfile = ({user}) => (
  <div className="user-profile">
    <div className="user-profile__photo">
      <img src={user.img} alt="" />
      <span className={user.online ? "online status" : "ofline status"}></span>
    </div>
    <p className="user-profile__name">{user.name}</p>
    <p className="user-profile__profession">{user.profession}</p>
    <p className="user-profile__about">{user.about}</p>
    <div>
      <p className="user-profile__info">Email</p>
      <p className="user-profile__desc">{user.email}</p>
      <p className="user-profile__info">Phone</p>
      <p className="user-profile__desc">{user.phone}</p>
      <p className="user-profile__info">Adress</p>
      <p className="user-profile__desc">{user.adress}</p>
      <p className="user-profile__info">Organization</p>
      <p className="user-profile__desc">{user.organization}</p>
    </div>
  </div>
);

export default UserProfile;