import React, { Component } from 'react';
// import MainHeader from '../../ui/components/MainHeader/MainHeader';
import './mail.css';

import Tabs from '../../ui/components/Tabs/Tabs';
import Tab from '../../ui/components/Tabs/Tab';
import Select from '../../ui/components/Select/Select';

import ListMessage from './Components/ListMessage/ListMessage';
import UserProfile from './Components/UserProfile/UserProfile';
import Chat from './Components/Chat/Chat';
// img
import Anastasia from '../../assets/img/chat/chat1.jpg';
import Alina from '../../assets/img/chat/alina.jpg';
import Roman from '../../assets/img/chat/chat3.jpg';
import Max from '../../assets/img/chat/chat4.jpg';
import Fifty from '../../assets/img/chat/50-cent.jpg';
import Eminem from '../../assets/img/chat/eminem.jpg';
import Tupac from '../../assets/img/chat/2pac.jpg';
import Eazy from '../../assets/img/chat/eazy.jpg';
import Dre from '../../assets/img/chat/Dre.jpg';


import Delete from 'react-icons/lib/md/delete';
import InboxIcon from 'react-icons/lib/md/move-to-inbox';
import SendIcon from 'react-icons/lib/md/send';

import '../../ui/components/HeaderSection/HeaderSection.css';



const optionSelect = ['По дате', 'Непрочитаные'];

class Mail extends Component {
  constructor(props) {
    super(props);
    const database = {
      inbox: [
        {
          name: 'Алина Атаджанова',
          id: '0001',
          online: false,
          profession: 'UX/UI Designer',
          about: 'Помогала в создании и развитии Material Design и возглавляла это направление. Она работал с совершенно разными командами Google, следя за соблюдением принципов Material Design.',
          img: Alina,
          email: 'alina@gmail.com',
          phone: '+1 650-253-0000',
          adress: '1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA',
          organization: 'Google Inc',
          read: true,
          messages: [
            {
              text: 'Hello my name is Alina',
              date: curentDate()
            }
          ],
          dialog: [
            {
              text: 'Hello my name is Alina',
              date: new Date().toLocaleString(),
              bot: true
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            }, {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            }

          ]
        },
        {
          name: 'Анастасия Катеринич',
          id: '0002',
          online: false,
          profession: 'Programmer',
          about: 'Ведущий инженером-программистом в проекте пилотируемой программы полетов к Луне «Аполлон»',
          img: Anastasia,
          email: 'anastasia@gmail.com',
          phone: '+44 2076 292 716',
          adress: '34 Old Bond St, Mayfair, W1S 4QL London',
          read: false,
          organization: 'NASA',
          messages: [
            {
              text: 'Hello my name is Anastasia',
              date: new Date('6/10/2018 12:46:03')
            }
          ],
          dialog: [
            {
              text: 'Hello my name is Anastasia',
              date: new Date().toLocaleString(),
              bot: true
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            }
          ]
        },
        {
          name: 'Роман Крапива',
          id: '0003',
          online: false,
          img: Roman,
          email: 'roman@gmail.com',
          phone: '+12 129 410 911',
          adress: '18 E Broadway, New York, NY 10002, USA',
          organization: 'Golden Unicorn',
          read: true,
          messages: [
            {
              text: 'Hello my name is Roman',
              date: new Date('6/01/2018 17:04:03')
            }
          ],
          dialog: [
            {
              text: 'Hello my name is Roman',
              date: new Date().toLocaleString(),
              bot: true
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            }
          ]
        },
        {
          name: 'Максим Шикунов',
          id: '0004',
          online: false,
          img: Max,
          email: 'max@gmail.com',
          phone: '+14 169 292 497',
          adress: '492 Parliament St, Toronto, ON M4X 1P2, CANADA',
          organization: 'Starbucks',
          read: false,
          messages: [
            {
              text: 'Hello my name is Max',
              date: new Date('6/02/2018 18:06:43')
            }
          ],
          dialog: [
            {
              text: 'Hello my name is Max',
              date: new Date().toLocaleString(),
              bot: true
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            }
          ]
        }
      ],
      sent: [
        {
          name: '50-Cent',
          id: '0005',
          online: false,
          profession: 'Рэпер, актёр',
          about: 'Рождённый в бедном негритянском районе Саут-Джамейка (англ.)русск. (Куинс, Нью-Йорк), Кёртис Джексон в возрасте 12 лет начал торговать кокаином. После того как он бросает торговлю кокаином, чтобы посвятить себя музыкальной карьере, в него стреляют 9 раз в 2000 году',
          img: Fifty,
          email: 'Curtis@gmail.com',
          phone: '+1 650-253-0000',
          adress: '1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA',
          organization: 'G-unit',
          messages: [
            {
              text: 'Hello my name is Alina',
              date: new Date('6/02/2018 20:05:09'),
              bot: false
            }
          ],
          dialog: [
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            },
            {
              text: 'Hello my name is 50-Cent',
              date: new Date().toLocaleString(),
              bot: true
            }
          ]
        },
        {
          name: 'Eminem',
          id: '0006',
          online: false,
          profession: 'Рэпер, музыкальный продюсер, композитор, актёр',
          img: Eminem,
          email: 'BillGates@gmail.com',
          phone: '+44 2076 292 716',
          about: 'Эминем является одним из самых продаваемых музыкальных артистов в мире, а также самым продаваемым артистом 2000-х.',
          adress: '34 Old Bond St, Mayfair, W1S 4QL London',
          organization: 'Shady',
          messages: [
            {
              text: 'Hello my name is Marshall Mathers',
              date: new Date('6/02/2018 15:42:11'),
              read: false
            }
          ],
          dialog: [
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            },
            {
              text: 'Hello my name is Eminem',
              date: new Date().toLocaleString(),
              bot: true
            }
          ]
        },
        {
          name: '2Pac',
          id: '0007',
          online: false,
          img: Tupac,
          profession: 'Хип-хоп исполнитель',
          email: 'Tupac@gmail.com',
          about: 'Является одним из величайших и наиболее влиятельных хип-хоп исполнителей в истории. По состоянию на 2007 год, Шакур продал более 75 миллионов записей по всему миру',
          phone: '+12 129 410 911',
          adress: '18 E Broadway, New York, NY 10002, USA',
          organization: 'Death Row',
          messages: [
            {
              text: 'Hello my name is Nazar',
              date: new Date('6/02/2018 11:50:17'),
              read: false
            }
          ],
          dialog: [
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            },
            {
              text: 'Hello my name is Tupac',
              date: new Date().toLocaleString(),
              bot: true
            }
          ]
        }
      ],
      trash: [
        {
          name: 'Eazy-E',
          id: '0009',
          online: false,
          profession: 'Рэпер',
          about: 'Более известный под своим сценическим псевдонимом Eazy-E Является крёстным отцом-основателем таких стилей, как Gangsta-Rap и G-Funk, и считается важнейшей фигурой в их популяризации. По состоянию на 2016 год, Eazy-E продал более 80 миллионов альбомов по всему миру',
          img: Eazy,
          email: 'Eazy_E@gmail.com',
          phone: '+1 650-253-0000',
          adress: '1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA',
          organization: 'NWA, Ruthless',
          messages: [
            {
              text: 'Hello my name is Alina',
              date: new Date('6/07/2018 22:11:47'),
              read: false
            }
          ],
          dialog: [
            {
              text: 'Hello my name is Alina',
              date: new Date().toLocaleString(),
              bot: true
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            }
          ]
        },
        {
          name: 'Dr. Dre',
          id: '0010',
          online: false,
          img: Dre,
          profession: 'Рэпер и продюсер',
          about: 'Американский рэпер и продюсер, один из наиболее успешных битмейкеров в рэп-музыке, Помимо сольного творчества, Dr. Dre продюсировал альбомы многочисленных рэперов, среди которых были Snoop Dogg, Eminem, 50 Cent, Xzibit, 2Pac, The Game и Busta Rhymes, многие альбомы которых стали мультиплатиновыми',
          email: 'Dr_Dre@gmail.com',
          phone: '+44 2076 292 716',
          adress: '34 Old Bond St, Mayfair, W1S 4QL London',
          organization: 'Interscope',
          messages: [
            {
              text: 'Hello my name is Anastasia',
              date: curentDate(),
              read: false
            }
          ],
          dialog: [
            {
              text: 'Hello my name is Anastasia',
              date: new Date().toLocaleString(),
              bot: true
            },
            {
              text: 'Hello my name is Nazar',
              date: new Date().toLocaleString(),
              bot: false
            }
          ]
        }
      ]
    };

    this.state = {
      database: database, // база информации
      typeChat: 'inbox', // активный тип чата
      messageTab: 'inbox',
      activeMessageList: 0, // активный элемент в списке сообщений
      messageIndex: 0,
      unread: 0, // количество не прочитанных входящих 
      typing: false
    };
  }

  handleChange = (index) => {
    let tabsIndex;

    if (index === 0) {
      tabsIndex = 'inbox';
    } else if (index === 1) {
      tabsIndex = 'sent';
    } else if (index === 2) {
      tabsIndex = 'trash';
    }

    this.setState({
      typeChat: tabsIndex,
      activeMessageList: 0
    }, () => {
      this.scrollChat();
      this.filterMassage(optionSelect[0]);
    });
  }

  // активный элемент из списка сообщений
  channgeMassageList = (index) => {
    console.dir(this.refs.chatMain);
    // тут меняем на read true
    const database = this.state.database;
    database[this.state.typeChat][index].read = true;

    this.calcUnread();

    this.setState({
      activeMessageList: index,
      ...database
    }, () => { this.scrollChat(); });
  }

  scrollChat = () => {
    // авто скролл чат
    this.chatBox.scrollTop = this.chatBox.scrollHeight;
  }

  // скрол чата вниз
  setChatScroll = (chat) => {
    this.chatBox = chat;
    this.scrollChat();
  }

  // новое сообщение от пользываля
  newMessage = (input) => {
    this.setState({
      messageIndex: this.state.activeMessageList,
      messageTab: this.state.typeChat
    }, () => {
      this.addMessage(input.value, false);
      input.value = "";
    });

    // проверяем что бот в онлайн
    if (this.state.database[this.state.typeChat][this.state.activeMessageList].online) {
      setTimeout(() => this.setState({ typing: true }), 2000);
      setTimeout(() => this.botMessage(), 5000);
    }
  }

  // сообщений от бота
  botMessage = () => {
    // тут read false
    // и найти span и дам сделать проверку какой сейчас read и добавить класс
    const randomMessage = randomQuestion();
    this.addMessage(randomMessage, true);
    this.setState((database) => {
      const obj = {
        text: randomMessage,
        date: curentDate(),
        read: false
      };

      const data = this.state.database;

      if (this.state.messageIndex !== this.state.activeMessageList) {
        data[this.state.typeChat][this.state.messageIndex].read = false;
      }


      this.calcUnread();
      data[this.state.messageTab][this.state.messageIndex].messages.push(obj);
      return {
        database: data,
        typing: false
      };
    });

  }

  // фильтрация сообщений по типу выбраном в селекте
  filterMassage = (e) => {
    const typeSort = typeof e === 'string' ? e : e.target.value;

    function sortUnread(user) {
      if (user.read) { return 1; }
    }

    function sortDate(a, b) {
      const date1 = a.messages[a.messages.length - 1].date.getTime();
      const date2 = b.messages[b.messages.length - 1].date.getTime();

      return date2 - date1;
    }

    function sortArray(callback) {

      this.setState(state => {
        const database = state.database[state.typeChat].sort(callback);
        return { ...database };
      }, this.scrollChat);
    }

    if (typeSort === 'Непрочитаные') {
      sortArray.call(this, sortUnread);

    } else if (typeSort === 'По дате') {
      sortArray.call(this, sortDate);
    }

  }

  // удалить сообщение
  deleteMessage = () => {
    const database = this.state.database;
    const userIndex = this.state.activeMessageList;
    const deleteUser = database[this.state.typeChat].splice(userIndex, 1);
    database["trash"].push(deleteUser[0]);

    this.setState({
      database: database,
      activeMessageList: 0
    });

    this.calcUnread();
  }

  // публикация сообщения
  addMessage = (text, boolean) => {
    const database = this.state.database;
    const date = new Date().toLocaleString();
    database[this.state.messageTab][this.state.messageIndex].dialog.push({
      text: text,
      // img: boolean ? User : imgUser[5],
      date: date,
      bot: boolean
    });

    this.setState({
      ...database
    }, () => this.scrollChat());

  }

  // посчитать количество непрочитаных сообщений
  calcUnread = () => {
    this.setState(state => {
      const unread = state.database.inbox.reduce((total, message) => total + (!message.read ? 1 : 0), 0);

      return { unread: unread };
    });
  }

  // показывает текст "пишит..." когда бот отвечает
  typing = () => {
    const currentBot = this.state.typing && this.state.messageIndex === this.state.activeMessageList && this.state.typeChat === this.state.messageTab;
    if (currentBot) {
      setTimeout(() => { this.scrollChat(); });
      return (
        <div className="bot-typing">{this.state.database[this.state.typeChat][this.state.messageIndex].name} печатает...</div>
      );
    }

    if (!this.state.database[this.state.typeChat][this.state.activeMessageList].online) {
      return (
        <div className="bot-typing">Не в сети</div>
      );
    }

  }

  // генерация онлайн рандом юзеров
  randomOnline = () => {
    const inbox = this.state.database['inbox'];
    const sent = this.state.database['sent'];
    const trash = this.state.database['trash'];

    const userInbox = generateNumber(inbox);
    const userSent = generateNumber(sent);
    const userTrash = generateNumber(trash);

    function setOnline(arrIndex, user) {
      for (let i = 0; i < arrIndex.length; i++) {
        user[arrIndex[i]].online = true;
      }

      return user;
    }

    const database = {
      inbox: setOnline(userInbox, inbox),
      sent: setOnline(userSent, sent),
      trash: setOnline(userTrash, trash)
    };

    this.setState({ ...database });
  }

  componentDidMount() {
    this.calcUnread();
    this.randomOnline();
    this.filterMassage(optionSelect[0]);
  }


  render() {
    return (
      <main className="mail">
        <header className="section-header">
          <Tabs onChange={this.handleChange}>
            <Tab label="Входящие" unread={this.state.unread}><InboxIcon size={20} /></Tab>
            <Tab label="Отправленные"><SendIcon size={20} /></Tab>
            <Tab label="Удаленные"><Delete size={20} /></Tab>
          </Tabs>
          <Select
            option={optionSelect}
            handleChange={this.filterMassage}
          />
        </header>

        <div ref="chatMain" className="chat-main">

          {this.state.database[this.state.typeChat].length > 0 ?
            <ListMessage
              data={this.state.database[this.state.typeChat]}
              currentMessage={this.state.activeMessageList}
              typeChat={this.state.typeChat}
              onClick={this.channgeMassageList}
              index={this.state.activeMessageList}
            /> : null}

          {this.state.database[this.state.typeChat].length > 0 ?
            <Chat
              messages={this.state.database[this.state.typeChat][this.state.activeMessageList].dialog}
              img={this.state.database[this.state.typeChat][this.state.activeMessageList].img}
              typing={this.typing}
              newMessage={this.newMessage}
              typeChat={this.state.typeChat}
              scrollChat={this.setChatScroll}
              deleteMessage={this.deleteMessage}
            /> : <div className="message-empty">Писем нет</div>
          }

          {this.state.database[this.state.typeChat].length > 0 ?
            <UserProfile
              user={this.state.database[this.state.typeChat][this.state.activeMessageList]}
            /> : null
          }

        </div>
      </main>
    );
  }
}

export default Mail;


// скролить верх по клику на активный диалог
// склонение месяцев
// добавить правельные даты у всех чатов
// сделать сообщение сегодня и вчера и дальше
// сохоанять все в node js
// в чат листе показывать последное сообщение не важно от кого


// увеличить высота чата чат
// заполнить сообщение а профильм юзеров 
// количество входящий не прочатаных должно быть без  моих ответов
// адрес в профиле гугл карта и емеил ссылка
// сделать правильное изменение state через prevstate

const botAnswer = [
  'Звонок в дверь, стоят двое: Здравствуйте, мы - Свидетели Иеговы! Да? А что, у него свадьба?',
  'Принесите мне, пожалуйста, графинчик водочки и что-нибудь на ваш вкус... Так и запишем: два графинчика водочки.',
  'Чтобы капусту не погрызли зайцы, ее рекомендуется выращивать на открытой, хорошо простреливаемой местности.',
  'Путь к сердцу мужчины лежит: для красивых женщин через желудок, для некрасивых - через печень.',
  'Для того чтобы измерить интеллект людей, сидящих с вами за одним столом, достаточно уронить вилку. Умные при этом тактично промолчат, а тупые обязательно сделают гениальное открытие: "Женщина придёт!"',
  'Если русский человек решил ничего не делать, то его не остановить.',
  ' Дорогая! Какой вкусный торт! Я его в магазине купила. А сама такой же испечёшь? Из чего? У нас нет ни глютамата натрия, ни Е517, ни Е1452!...',
  'Из разговора двух хирургов: Ну как прошла операция? Да, ерунда. С наркозом вообще скучновато...',
  'Объявление: «Требуются молодые, инициативные люди. Оплата высокая. Не работа!»',
  'Сынок, мы с папой давно хотели тебя спросить: откуда ты взялся???',
  'Начинающий бизнесмен - приятелю: Когда я открою свою фирму, я обязательно назову ее ООО "ООО". А потом буду смотреть, как секретари отвечают на телефонные звонки.',
  'Зачастую разговор со службой техподдержки интернет-провайдера складывается так, что становится непонятно - кто кому оказывает помощь.',
  'Реклама: "Галина Бланка" готовит вам сюрприз! Напишите нам, за что вы любите "Галину Бланка", вложите в конверт две обертки от бульонных кубиков, и через месяц вы можете получить ЧЕТЫРЕ обертки, еще через месяц - ВОСЕМЬ!',
  'Я долго думал, что такое 90х60х90. Оказалось, что это 486000!',
  'Новая акция от роддома. Роди двойню и получи третьего в подарок!',
  'Две кошки стоят перед клеткой с попугаями: Чур я беру красного! – говорит одна. Почему это? – обалдевает другая. Ну, просто зелёный ещё недозрелый, я думаю...',
  'Слушай, а этот коллайдер с Байконура запускают?',
  'Нещасний випадок трапився на фестивалі пива. В гараж зайшла дружина ...',
  'Олень, зацепившись рогами за провода, побежал по 13-му маршруту.',
  'Мальчик  не прибирался в комнате, пока не споткнулся о бомжа.'
];

// случаное сообщение бота
function randomQuestion() {
  const len = botAnswer.length - 1;
  const randomNumber = Math.round(Math.random() * len);
  return botAnswer[randomNumber];
}

// текущее время
// function curentTime() {
//   const time = new Date();
//   return time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
// }


// настя
// Путь к сердцу мужчины лежит: для красивых женщин через желудок, для некрасивых - через печень.
// Реклама: "Галина Бланка" готовит вам сюрприз! Напишите нам, за что вы любите "Галину Бланка", вложите в конверт две обертки от бульонных кубиков, и через месяц вы можете получить ЧЕТЫРЕ обертки, еще через месяц - ВОСЕМЬ!


// генаратор случайных уникальных числев для массива
function generateNumber(userList) {
  const randomArr = [];
  while (true) {
    if (randomArr.length === Math.floor(userList.length / 2)) break;
    const randomNumber = Math.floor(1 + Math.random() * userList.length - 1);
    if (randomArr.indexOf(randomNumber) === -1) {
      randomArr.push(randomNumber);
    }
  }

  return randomArr;
}


function curentDate() {
  var now = new Date();
  var date = new Date(
    now.getFullYear(),
    now.getMonth(), now.getDate(),
    now.getHours(), now.getMinutes(),
    now.getSeconds()
  );

  return date;
}



