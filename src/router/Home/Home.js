import React, { Component } from 'react';
import Calendar from 'react-calendar';
import ReactHighcharts from 'react-highcharts';

// config for chart
import areaConfig from '../../config/chart/area.config';
import colomnConfig from '../../config/chart/column.config';

import Select from '../../ui/components/Select/Select';
import './Home.css';


const optionSelect = ['Year', 'Month', 'Week', 'Day'];

const columnSettings = {
  year: [200, 800, 600, 400, 300, 500, 200, 800, 500, 600, 200, 105],
  month: [1000, 400, 200, 700, 150, 100, 140, 400, 800, 200, 1000, 500],
  week: [300, 525, 457, 100, 999, 854, 240, 800, 400, 527, 1471, 450],
  day: [500, 100, 850, 500, 400, 300, 800, 100, 250, 454, 600, 250]
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  changeColumnChart = (e) => {
    const period = e.target.value.toLowerCase();
    const chart = this.refs.chartColumn.getChart();
    chart.series[0].setData(columnSettings[period]);
  }

  render() {
    return (
      <main className="home">
        <div className="graph">
          <ReactHighcharts config={areaConfig} />
          <div className="column-chart">
            <header className="chart-header">
              <h2>Отчет о продажах</h2>
              <Select option={optionSelect} handleChange={this.changeColumnChart} />
            </header>
            <ReactHighcharts config={colomnConfig} ref="chartColumn" />
          </div>
        </div>
        <Calendar value={new Date()} locale="ru-Ru" />
      </main>
    );
  }
}

export default Home;