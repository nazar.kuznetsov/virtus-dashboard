import React, { Component } from 'react';
import './authentication.css';
import logo from '../../assets/img/logo.png';



class Authorization extends Component {
  register = (e) => {
    e.preventDefault();

    const user = {
      email: this.email.value,
      password: this.password.value,
      name: this.name.value,
      phone: this.phone.value
    };

    if (user.name.length === 0 || user.password.length === 0 ) return false;

    fetch('/api/register', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(user)
    })
      .then(res => { return res.json() ;})
      .then((res) => {
        console.log(res);
      });
  };

  render() {
    return (
      <div className="authentication">
        <div className="authentication-form">
          <div className="form-header">
            <img src={logo} alt="Virtus" />
          </div>
          <form className="form" action="/user" onSubmit={this.register}>
            <input ref={e => this.name = e} className="input" type="text" placeholder="Имя" />
            <input ref={e => this.password = e} className="input" type="password" placeholder="Пароль" />
            <input ref={e => this.email = e} className="input" type="text" placeholder="Почта" />
            <input ref={e => this.phone = e} className="input" type="text" placeholder="Телефон" />
            <button className="button">Зарегистрироваться</button>
          </form>
          <a className="authentication-link" href="/login">Уже есть аккаунт?</a>
        </div>
      </div>
    );
  }
}

export default Authorization;

