import React, { Component } from 'react';
import './authentication.css';



class Login extends Component {

  login = (e) => {
    e.preventDefault();

    const user = {
      login: this.userEmail.value,
      password: this.userPassword.value
    };

    if (user.login.length === 0 || user.password.length === 0) return false;

    fetch('/api/login', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        login: user.login,
        password: user.password
      })
    })
      .then(res => { return res.json(); })
      .then((res) => {
        localStorage.setItem('authorized', JSON.stringify(res));

        if (res.authorized) {
          window.location.href = '/';
        }
      });
  }

  render() {
    return (
      <div className="authentication">
        <div className="authentication-form">
          <div className="form-header">
          </div>
          <form className="form" action="/api/login" onSubmit={this.login}>
            <input className="input" type="text" ref={e => this.userEmail = e} placeholder="Логин" />
            <input className="input" type="password" ref={e => this.userPassword = e} placeholder="Пароль" />
            <button className="button">Войти</button>
          </form>
          <a className="authentication-link" href="/register">Создать аккаунт</a>
        </div>
      </div>
    );
  }
}

export default Login;