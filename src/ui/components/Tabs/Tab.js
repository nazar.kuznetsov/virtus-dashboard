import React from 'react';


const Tab = (props) => (
  <li
    onClick={props.onClick}
    className={props.activeClass ? 'tab-item active' : 'tab-item'}>
    {props.label}
    {props.children ? <span className="tab-icon">{props.children}</span> : null}
    {props.label === 'Входящие' ? props.unread > 0 ? <span className="count-unread">({props.unread})</span> : '' : null}
  </li>
);

export default Tab;