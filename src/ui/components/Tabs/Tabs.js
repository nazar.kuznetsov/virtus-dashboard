import React, { Component } from 'react';
import Tab from './Tab';
import './Tab.css';


class Tabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0
    };
  }

  setIndex = (index) => {
    this.setState({
      tabIndex: index
    }, () => {
      this.props.onChange(index);
    });
  };

  activeClass = (index) => {
    return this.state.tabIndex === index;
  };


  render() {
    return (
      <ul className="tabs">
        {this.props.children.map((item, index) => {
          return (
            <Tab
              onClick={this.setIndex.bind(this, index)}
              label={item.props.label} key={index}
              activeClass={this.activeClass(index)}
              children={item.props.children}
              unread={item.props.unread}
            />
          );
        })}
      </ul>
    );
  }
}

export default Tabs;