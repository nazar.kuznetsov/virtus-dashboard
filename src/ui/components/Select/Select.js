import React from 'react';



const Select = ({ option, handleChange}) => (
  <select onChange={handleChange}>
    {option.map((item, index) => {
      return <option key={index} name={item} value={item}>{item}</option>;
    })}
  </select>
);

export default Select;