import React, { Component } from 'react';
import './header.css';
import logo from '../../assets/img/logo.png';

import FaAngleDown from 'react-icons/lib/fa/angle-down';

import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null
    };
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  logout = () => {
    this.setState({ anchorEl: null });
    localStorage.removeItem('authorized');
    window.location.reload();
  }



  render() {
    const {anchorEl} = this.state;
    return (
      <header className="header">
        <img src={logo} alt="Virtus" />
        <div className="user-panel">
          <button>Add</button>
          <div>
            <img src="" alt="user" />

            <Button
              aria-owns={anchorEl ? 'simple-menu' : null}
              aria-haspopup="true"
              onClick={this.handleClick}>
              <FaAngleDown />
            </Button>

            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.handleClose}>
              <MenuItem onClick={this.handleClose}>Настройки</MenuItem>
              <MenuItem onClick={this.logout}>Выход</MenuItem>
            </Menu>

          </div>
        </div>
      </header>
    );
  }
}

export default Header;