import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
// Router
import Home from '../router/Home/Home';
import Mail from '../router/Mail/Mail';
import Settings from '../router/Settings/Settings';
import Raport from '../router/Raport/Raport';
import Workflow from '../router/Workflow/Workflow';
import Login from '../router/Authentication/Login';
import Register from '../router/Authentication/Register';
import NoFound from '../router/NoFound/NoFound';
// Component
import Nav from '../ui/Nav/Nav';
import Header from '../ui/Header/Header';

import 'reset-css';
import './App.css';

const PrivatePage = ({ component: Component, data }) => {
  return (
    <Route render={() => {
      const authorization = JSON.parse(localStorage.getItem('authorized'));

      if (!authorization) {
        return <Redirect to="/login" />;
      }

      return (
        <div className="app">
          <Header />
          <div className="wrapper">
            <Nav />
            <Component data={data} />
          </div>
        </div>
      );
    }} />
  );
};


class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <PrivatePage exact path="/" component={Home} />
        <PrivatePage exact path="/workflow" component={Workflow} />
        <PrivatePage exact path="/report" component={Raport} />
        <PrivatePage exact path="/mail" component={Mail} />
        <PrivatePage exact path="/settings" component={Settings} />
        <Route component={NoFound} />
      </Switch>
    );
  }
}

export default App;