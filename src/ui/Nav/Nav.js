import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './nav.css';
import {
  MdMenu,
  MdHome,
  MdMail,
  MdPerson,
  MdShowChart
} from 'react-icons/lib/md';

const iconSize = 30;

class Nav extends Component {
  componentDidMount() {

    setTimeout(() => {
      this.refs.notification.classList.add('notification');
    }, 3000);

  }


  render() {
    return (
      <nav className="nav">
        <ul>
          <li className="nav__item"><NavLink exact={true} activeClassName="active" className="nav__link" to="/"><MdHome size={iconSize} /></NavLink></li>
          <li className="nav__item"><NavLink activeClassName="active" className="nav__link" to="/workflow"><MdMenu size={iconSize} /></NavLink></li>
          <li className="nav__item"><NavLink activeClassName="active" className="nav__link" to="/report"><MdShowChart size={iconSize} /></NavLink></li>
          <li ref="notification" className="nav__item"><NavLink activeClassName="active" className="nav__link" to="/mail"><MdMail size={iconSize} /></NavLink></li>
          <li className="nav__item"><NavLink activeClassName="active" className="nav__link" to="/settings"><MdPerson size={iconSize} /></NavLink></li>
        </ul>
      </nav>
    );
  }
}

export default Nav;




const users = [
  {
    login: 'admin',
    password: '1111',
    phone: '+48500400300',
    email: 'nazar.kuznettsov@gmail.com'
  }
];

const isLogin = users.some(item =>  item.login === 'admin');

console.log(isLogin);