const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

const users = [
  {
    login: 'admin',
    password: '1111',
    phone: '+48500400300',
    email: 'nazar.kuznettsov@gmail.com'
  }
];


// =================авторизация=========================
app.post('/api/login', function(req, res) {
  const login = req.body.login;
  const password = req.body.password;

  const user = users.filter(element => element.login === login)[0];


  if (user.login !== login  || user.password !== password) {
    return res.json({
      authorized: false
    });
  }

    return res.json({
      authorized: true,
      userName: login
    });

});


// =================регестрация=========================
app.post('/api/register', function(req, res) {
  const user = {
    login: req.body.name,
    password: req.body.password,
    phone: req.body.phone,
    email: req.body.email
  };

  // const isLogin = users.some(item =>  item.login === user.login);

  // if (isLogin) {
  //   return res.json(`Пользыватель с именем ${user.login} уже сущуствует!`);
  // }

  for (let i = 0; i < users.length; i++) {
    if (users[i].login === user.login) {
      return res.json(`Пользыватель с именем ${user.login} уже сущуствует!`);
    }
  }

  users.push(user);

  return res.json('Вы успешно зарагестрировались');

});


app.listen(5000, function() {
  console.log('Сервер успешно запущен порт 5000');
});