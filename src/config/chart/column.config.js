const column = {

  chart: {
    type: 'column',
    margin: [0, 0, 30, 0],
    backgroundColor: '#2f3242',
    events: {
      load: function(e) {
       // console.log(e.target.axes[1]);
      }
    }
  },
  credits: { enabled: false },
  title: {
    align: 'left',
    text: null,
    style: {
      color: '#ffffff',
      fontSize: '24px',
      fontFamily: 'Montserrat'
    }
  },
  xAxis: {
    tickWidth: 0,
    lineColor: '#505464',
    minPadding: .1, // This doesn't work
    categories: [
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      '10',
      '11',
      '12'
    ],
    labels: {
      rotation: 0,
      align: 'right',
      style: {
        fontSize: '16px',
        fontFamily: 'Montserrat',
        color: '#9ca1b2',
        fontWeight: '700'
      }
    }
  },
  yAxis: {
    gridLineWidth: 1, // ширина сетки
    gridLineColor: '#505464', // цвет сетки
    labels: {
      enabled: false
    },
    title: {
      text: null
    }
  },
  plotOptions: {
    column: {
      pointPadding: 0
    },
    series: {
      cursor: 'pointer'
  }
  },
  legend: {
    enabled: false
  },
  tooltip: {
  },
  series: [{
    color: '#505464',
    states:{
      hover: {
        color: '#2196f3'
    }
  },
    borderWidth: 0,
    name: 'Sales report',
    data: [200, 800, 600, 400, 300, 500, 200, 800, 500, 600, 200, 105],
    dataLabels: {
      enabled: false
    }
  }]
};

export default column;