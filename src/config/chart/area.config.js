const AreaSpline = {
chart: {
  type: 'areaspline',
  margin: 0, // оступ
  backgroundColor: '#2f3242' // фон
},

credits: {
  enabled: false // прячем рекламу
},
plotOptions: {
  areaspline: {
    lineWidth: 5 // ширина линий
  },
      series: {
        stacking: 'percent',
        color: '#284b70', // цвет волны
        lineColor: '#2196f3',// цвет сверху волны линий 
        point: {
          events: { // собития при наведение на секцию
              mouseOver: function(e) {
               document.querySelectorAll('.highcharts-axis-labels')[0].children[e.target.index - 1].classList.add('label-active');
              },
              mouseOut: function(e) {
                document.querySelectorAll('.highcharts-axis-labels')[0].children[e.target.index - 1].classList.remove('label-active');
              }
          }
      },
        marker: {
          radius: 8,
          lineWidth: 4,
          fillColor: '#ffffff',
          lineColor: '#2196f3',
          symbol: '../assets/img/circle.svg', // маркер картинкой
          enabled: false
        }
      }
    },
    series: [{
     color: '#2f3242',
      fillColor: '#2f3242',
      data: [7, 6, 5, 4, 3, 2, 1], // хз нао надо что бы было
      enableMouseTracking: false, // прячем верхний маркер что бы не дублировать
      linkedTo: 'main',
      lineColor: '#2B2D3C'
    }, {
    //	id: 'main',
     data: [10, 20, 3, 40, 5, 6, 0] // кордлинаты волны
    }],
    xAxis: {
      min: 1, // с какой колонки показывать
      max: 5, // до какой колонки
      gridLineWidth: 1, // ширина сетки
      gridLineColor: '#495f7c', // цвет сетки
      gridZIndex: 5, // индекс сетки
      categories: ['', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПН', 'СБ', 'ВС', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC', ''],
      labels: { // подписи снизу
        y: -30, // оступ снизу на 30px
        style: {
          color: '#9ca1b2',
          fontSize: '18px',
          letterSpacing: '0.32px',
          fontFamily: 'Montserrat',
          fontWeight: '400'
        }
      },
      crosshair: { // подстветка по секциям
        color: '#2196f3'
      }
    },
    yAxis: {
      title: {
        text: ''
      },
      gridLineWidth: '0',
      visible: false // прячем боковую линию
    },
    legend: {
      enabled: false // прячет легенды сбоку
    }
};

export default AreaSpline;